package me.miguelmendez.blog.model

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import com.natpryce.hamkrest.hasElement
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on

object BlogSpec: Spek({
    describe("a blog"){
        val blog = Blog(name = "Miguel's blog", tagLine = "Super duper blog")

        on("initialization"){

            it("should have a blog named 'Miguel's blog'"){
                assertThat(blog.name, equalTo("Miguel's blog"))
            }

            it("should have a blog with tag line 'Super duper blog'"){
                assertThat(blog.tagLine, equalTo("Super duper blog"))
            }
        }

        on("creating a blog entry draft"){
            val slug = blog.createBlogEntryDraft(title = "new entry", content = "Lorem Ipsum blah blah")
            blog.createBlogEntryDraft(title = "new entry2", content = "Lorem Ipsum blah blah")
            val blogEntry = blog.getBlogEntry(slug)
            assertThat(blog.blogEntries, hasElement(blogEntry))
            assertThat(blogEntry.status, equalTo(PostStatus.DRAFT))
        }

        on("publishing a blog entry"){
            blog.publishBlogEntry("new_entry")

            val publishedBlogEntry = blog.getBlogEntry("new_entry")
            assertThat(publishedBlogEntry.status, equalTo(PostStatus.PUBLISHED))
        }

        on("retiring a blog entry"){
            blog.retireBlogEntry("new_entry")

            val publishedBlogEntry = blog.getBlogEntry("new_entry")
            assertThat(publishedBlogEntry.status, equalTo(PostStatus.RETIRED))
        }

        on("adding an image to the media library"){}

        on("setting an image from the media library as the main image of a blog entry"){}

    }
})