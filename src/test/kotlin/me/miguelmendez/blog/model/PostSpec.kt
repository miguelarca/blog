package me.miguelmendez.blog.model

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import com.natpryce.hamkrest.hasElement
import com.natpryce.hamkrest.hasSize
import com.natpryce.hamkrest.isEmpty
import java.util.*

object PostSpec: Spek({
    describe("a blog entry"){
        val blogEntry = BlogEntry("entry title", "entry content")

        on("initialization"){

            it("should have title 'entry title'"){
                assertThat("entry title", equalTo(blogEntry.title))
            }

            it("should have content 'post content'"){
                assertThat("entry content", equalTo(blogEntry.content))
            }

            it("should have a status of 'DRAFT'"){
                assertThat(blogEntry.status, equalTo(PostStatus.DRAFT))
            }

            it("should have an empty set of comments"){
                assertThat(blogEntry.comments, isEmpty)
            }
        }

        on("adding a comment"){
            blogEntry.addComment(Comment("new comment"))

            it("should have only one comment"){
                assertThat(blogEntry.comments, hasSize(equalTo(1)))
            }

            it("should have the comment 'new comment'"){
                assertThat(blogEntry.comments, hasElement(Comment("new comment")))
            }
        }

        on("approving a comment"){
            blogEntry.addComment(Comment("new comment"))
            blogEntry.approveComment(Comment("new comment"))

            it("should have the comment 'new comment' with status APPROVED"){
                assertThat(blogEntry.comments, hasElement(Comment("new comment", CommentStatus.APPROVED)))
            }
        }

        on("rejecting a comment"){
            blogEntry.addComment(Comment("new comment"))
            blogEntry.rejectComment(Comment("new comment"))

            it("should have the comment 'new comment' with status APPROVED"){
                assertThat(blogEntry.comments, hasElement(Comment("new comment", CommentStatus.REJECTED)))
            }
        }

        on("publishing an entry"){
            blogEntry.publishEntry()

            it("should a have the 'PUBLISHED' status"){
                assertThat(blogEntry.status, equalTo(PostStatus.PUBLISHED))
            }
        }

        on("retiring an entry"){
            blogEntry.retireEntry()

            it("should a have the 'RETIRED' status"){
                assertThat(blogEntry.status, equalTo(PostStatus.RETIRED))
            }
        }
    }
})