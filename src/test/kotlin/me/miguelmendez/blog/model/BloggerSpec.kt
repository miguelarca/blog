package me.miguelmendez.blog.model

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.*

object BloggerSpec: Spek({
    describe("a blogger"){
        val blog = Blog(name = "Miguel's blog", tagLine = "Super duper blog")
        val blogger = Blogger(name = "Miguel Mendez", email = "miguel@superduperfly.com", blog = blog)

        on("initialization"){
            it("should be named 'Miguel Mendez'"){
                assertThat(blogger.name, equalTo("Miguel Mendez"))
            }

            it("should have the email 'miguel@superduperfly.com'"){
                assertThat(blogger.email, equalTo("miguel@superduperfly.com"))
            }
        }
    }
})