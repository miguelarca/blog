package me.miguelmendez.blog.model

import java.time.Instant

data class Comment(val content: String, val status: CommentStatus = CommentStatus.IN_REVIEW)
enum class CommentStatus{
    IN_REVIEW,
    APPROVED,
    REJECTED
}
enum class PostStatus{
    DRAFT,
    PUBLISHED,
    RETIRED
}

class BlogEntry(val title: String, val content: String){
    val createdAt = Instant.now()
    val slug = slugify(title)
    val comments: MutableSet<Comment> = mutableSetOf()
    var status: PostStatus = PostStatus.DRAFT
        private set

    fun publishEntry(){
        status = PostStatus.PUBLISHED
    }

    fun retireEntry(){
        status = PostStatus.RETIRED
    }

    fun addComment(newComment: Comment){
        comments.add(newComment)
    }

    fun approveComment(comment: Comment){
        if(comments.contains(comment)){
            comments.remove(comment)
            comments.add(comment.copy(status = CommentStatus.APPROVED))
        }
    }

    fun rejectComment(comment: Comment){
        if(comments.contains(comment)){
            comments.remove(comment)
            comments.add(comment.copy(status = CommentStatus.REJECTED))
        }
    }

    private fun slugify(title: String) = title
            .trim()
            .replace(" ", "_")
            .replace(",", "-")
            .toLowerCase()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BlogEntry

        if (content != other.content) return false
        if (slug != other.slug) return false

        return true
    }

    override fun hashCode(): Int {
        var result = content.hashCode()
        result = 31 * result + slug.hashCode()
        return result
    }


}