package me.miguelmendez.blog.model

import java.net.URI
import java.util.*

data class Media(val id: UUID, val size: Int, val type: MediaType, val location: URI)
class Blogger(val name: String, val email: String, private val blog:Blog)

enum class MediaType{
    IMAGE,
    VIDEO,
    STREAM
}

data class Blog(val name: String, val tagLine: String){
    val mediaCollection: MutableSet<Media> = mutableSetOf()
    val blogEntries: MutableSet<BlogEntry> = mutableSetOf()

    fun createBlogEntryDraft(title: String, content:String ): String {
        val blogEntry = BlogEntry(title, content)
        blogEntries.add(blogEntry)

        return blogEntry.slug
    }

    fun getBlogEntry(slug: String) = blogEntries.first { it.slug == slug }

    fun publishBlogEntry(slug: String){
        blogEntries.first { it.slug == slug }.publishEntry()
    }
    
    fun retireBlogEntry(slug: String){
        blogEntries.first { it.slug == slug }.retireEntry()
    }

}