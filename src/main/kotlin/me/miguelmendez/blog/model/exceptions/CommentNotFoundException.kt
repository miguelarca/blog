package me.miguelmendez.blog.model.exceptions

class CommentNotFoundException(message: String = "Comment was not found") : RuntimeException(message)